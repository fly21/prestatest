<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'prestashop/prestashop',
  ),
  'versions' => 
  array (
    'beberlei/doctrineextensions' => 
    array (
      'pretty_version' => 'v1.2.6',
      'version' => '1.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af72c4a136b744f1268ca8bb4da47a2f8af78f86',
    ),
    'behat/behat' => 
    array (
      'pretty_version' => 'v3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08052f739619a9e9f62f457a67302f0715e6dd13',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51ac4500c4dc30cbaaabcd2f25694299df666a31',
    ),
    'behat/transliterator' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.2.6',
      'version' => '1.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '47fe531de31fca4a1b997f87308e7d7804348f7e',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b93bcf0fa1fccb0b7d176b0967d969691cd74cca',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6bea70230ef4dd483e6bbcab6005f682ed3a8de',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cbe23383749496fe0f373345208b79568e4bc248',
    ),
    'csa/guzzle-bundle' => 
    array (
      'pretty_version' => 'dev-compat-php',
      'version' => 'dev-compat-php',
      'aliases' => 
      array (
      ),
      'reference' => '7282d89834960347c830efd43dcd788f09a9959a',
    ),
    'cssjanus/cssjanus' => 
    array (
      'pretty_version' => 'dev-patch-1',
      'version' => 'dev-patch-1',
      'aliases' => 
      array (
      ),
      'reference' => '7866b8a6f7ad8ba8c7f4eb9f87b084452a41d8e5',
    ),
    'curl/curl' => 
    array (
      'pretty_version' => '1.9.3',
      'version' => '1.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '43436a604b18c2a985cbc3f983be817fbe500e99',
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c6fea3d9a4eaaa8cef86b2a89f3660818117b33',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '904dca4eb10715b92569fbcd79e201d5c349b6bc',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '382e7f4db9a12dc6c19431743a2b096041bcdd62',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.4',
      'version' => '1.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b1e4b2b66f6d6e49983cebfe23a21b7ccc5b0d7',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '2.12.0',
      'version' => '2.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2053eafdf60c2172ee1373d1b9289ba1db7f1fc6',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => 'v2.9.3',
      'version' => '2.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7345cd59edfa2036eb0fa4264b77ae2576842035',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '1.12.6',
      'version' => '1.12.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '08f944760ac471aa97a9b0386dfdb559db80b32d',
    ),
    'doctrine/doctrine-cache-bundle' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6bee2f9b339847e8a984427353670bad4e7bdccb',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '629572819973f13486371cb611386eb17851e85c',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec3a55242203ffa6a4b27c58176da97ff0a7aec1',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae466f726242e637cebdd526a7d991b9433bacf1',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1febd6c3ef84253d7c815bed85fc622ad207a9f8',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => 'v2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d763ca4c925f647b248b9fa01b5f47aa3685d62',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '1.3.5',
      'version' => '1.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be70c016fdcd44a428405ee062ebcdd01a6867cd',
    ),
    'doctrine/reflection' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc420ead87fdfe08c03ecc3549db603a45b06d4c',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.15',
      'version' => '2.1.15.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e834eea5306d85d67de5a05db5882911d5b29357',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.12.0',
      'version' => '4.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a617e55bc62a87eec73bd456d146d134ad716f03',
    ),
    'fig/link-util' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '47f55860678a9e202206047bc02767556d298106',
    ),
    'friendsofphp/php-cs-fixer' => 
    array (
      'pretty_version' => 'v2.16.4',
      'version' => '2.16.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1023c3458137ab052f6ff1e09621a721bfdeca13',
    ),
    'friendsofsymfony/jsrouting-bundle' => 
    array (
      'pretty_version' => '2.5.3',
      'version' => '2.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '165b3f4ae1f798ec320b9942f6db921f7b568bed',
    ),
    'geoip2/geoip2' => 
    array (
      'pretty_version' => 'v2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b28a0ed0190cd76c878ed7002a5d1bb8c5f4c175',
    ),
    'greenlion/php-sql-parser' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0262c0848d95c7f777d1a5aa6b0a42e7763e223',
    ),
    'guzzlehttp/cache-subscriber' => 
    array (
      'pretty_version' => '0.2.0',
      'version' => '0.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c766ba399e4c46383e3eaa220201be62abd101e',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '5.3.4',
      'version' => '5.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b87eda7a7162f95574032da17e9323c9899cb6b2',
    ),
    'guzzlehttp/log-subscriber' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99c3c0004165db721d8ef7bbef60c996210e538a',
    ),
    'guzzlehttp/ringphp' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e2a174052995663dd68e6b5ad838afd47dd615b',
    ),
    'guzzlehttp/streams' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '47aaa48e27dae43d39fc1cea0ccf0d84ac1a2ba5',
    ),
    'incenteev/composer-parameter-handler' => 
    array (
      'pretty_version' => 'v2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '933c45a34814f27f2345c11c37d46b3ca7303550',
    ),
    'ircmaxell/password-compat' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c5cde8822a69545767f7c7f3058cb15ff84614c',
    ),
    'ircmaxell/random-lib' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e9e0204f40e49fa4419946c677eccd3fa25b8cf4',
    ),
    'ircmaxell/security-lib' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3db6de12c20c9bcd1aa3db4353a1bbe0e44e1b5',
    ),
    'jakeasmith/http_build_url' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '93c273e77cb1edead0cf8bcf8cd2003428e74e37',
    ),
    'jdorn/sql-formatter' => 
    array (
      'pretty_version' => 'v1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
    ),
    'johnkary/phpunit-speedtrap' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dac11b8640d4be7a70f336616947fa84f169835a',
    ),
    'league/tactician' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0339e22fd9252fb0fa53102b488d2c514483b8a',
    ),
    'league/tactician-bundle' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '66b97472915290b93654e60610295660f597ad4e',
    ),
    'league/tactician-container' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1a5d884e072b8cafbff802d07766076eb2ffcb0',
    ),
    'league/tactician-logger' => 
    array (
      'pretty_version' => 'v0.10.0',
      'version' => '0.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ff9ee04e4cbec100af827f829ed4c7ff7c08442',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '1.4.7',
      'version' => '1.4.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ea674a8308baf547cbcbd30c5fcd6d301b7c000',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5348c5a67e3b75cd209d70103f916a93b1f1ed21',
    ),
    'martinlindhe/php-mb-helpers' => 
    array (
      'pretty_version' => '0.1.6',
      'version' => '0.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '3550df9e6976f753d2879543e4aaf96eaa9657a2',
    ),
    'matthiasmullie/minify' => 
    array (
      'pretty_version' => '1.3.62',
      'version' => '1.3.62.0',
      'aliases' => 
      array (
      ),
      'reference' => '47a53716f94139aff22922ffd73283ff04f23cdf',
    ),
    'matthiasmullie/path-converter' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e4b121c8b9f97c80835c1d878b0812ba1d607c9',
    ),
    'maxmind-db/reader' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'febd4920bf17c1da84cef58e56a8227dfb37fbe4',
    ),
    'maxmind/web-service-common' => 
    array (
      'pretty_version' => 'v0.6.0',
      'version' => '0.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '40c928bb0194c45088b369a17f9baef9c3fc7460',
    ),
    'mikey179/vfsstream' => 
    array (
      'pretty_version' => 'v1.6.8',
      'version' => '1.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '231c73783ebb7dd9ec77916c10037eff5a2b6efe',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.34',
      'version' => '2.8.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f8113f57a508494ca36acbcfa2dc2d923c7ed5b',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.25.3',
      'version' => '1.25.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa82921994db851a8becaf3787a9e73c5976b6f1',
    ),
    'mrclay/minify' => 
    array (
      'pretty_version' => '2.3.3',
      'version' => '2.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1928e89208d28e91427b2f13b67acdbd8cd01ac9',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.9.4',
      'version' => '1.9.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '579bb7356d91f9456ccd505f24ca8b667966a0a7',
      'replaced' => 
      array (
        0 => '1.9.4',
      ),
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9981c347c5c49d6dfe5cf826bb882b824080dc',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v2.0.18',
      'version' => '2.0.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a58ef6e3146256cc3dc7cc393927bcc7d1b72db',
    ),
    'pear/archive_tar' => 
    array (
      'pretty_version' => '1.4.11',
      'version' => '1.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '17d355cb7d3c4ff08e5729f29cd7660145208d9d',
    ),
    'pear/console_getopt' => 
    array (
      'pretty_version' => 'v1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a41f8d3e668987609178c7c4a9fe48fecac53fa0',
    ),
    'pear/pear-core-minimal' => 
    array (
      'pretty_version' => 'v1.10.10',
      'version' => '1.10.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '625a3c429d9b2c1546438679074cac1b089116a7',
    ),
    'pear/pear_exception' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbb42a5a0e45f3adcf99babfb2a1ba77b8ac36a7',
    ),
    'pelago/emogrifier' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2472bc1c3a2dee8915ecc2256139c6100024332f',
    ),
    'phake/phake' => 
    array (
      'pretty_version' => 'v3.1.7',
      'version' => '3.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b7a6db62dfe7015a480fa966967df28b3cb239d',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'php-cs-fixer/diff' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '78bb099e9c16361126c86ce82ec4405ebab8e756',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '63a995caa1ca9e5590304cd845c15ad6d482a62a',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '4.3.4',
      'version' => '4.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da3fd972d6bafd628114f7e7e036f45944b62e9c',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2e32a6d48972b2c1976ed5d8967145b6cec4a4a9',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1648dc9ebef6ebe0c5a172e16cf66732918416e0',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cbe1df668b3fe136bcc909126a0f529a78d4cbbc',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '6.1.4',
      'version' => '6.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '7.5.20',
      'version' => '7.5.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9467db479d1b0487c99733bb1e7944d32deded2c',
    ),
    'prestashop/blockreassurance' => 
    array (
      'pretty_version' => 'v5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba730eb184e6e68b870b9f91f4a1f2a56518127a',
    ),
    'prestashop/circuit-breaker' => 
    array (
      'pretty_version' => 'v3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8764540d470b533c9484534343688733bc363f77',
    ),
    'prestashop/contactform' => 
    array (
      'pretty_version' => 'v4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '849aae54ec564aca94877b9bee50e71bb0468edb',
    ),
    'prestashop/dashactivity' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d41fab7a58cdaeabc0248f6fd571da32d7615d4',
    ),
    'prestashop/dashgoals' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a8d41bcc9f2b09924b5e82407bcd6d61ea61ab6',
    ),
    'prestashop/dashproducts' => 
    array (
      'pretty_version' => 'v2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '22652ff141bfc3d5563918210f9b58305c80f4d7',
    ),
    'prestashop/dashtrends' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da68ea81c2d0d13d9fd851934c75927fe222d5e3',
    ),
    'prestashop/decimal' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b7f47671bb691bd463743d1cbab5b519fbd6849',
    ),
    'prestashop/graphnvd3' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4b8fa0d82d047b63be02e74e84a27f46a746065',
    ),
    'prestashop/gridhtml' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbd3985d5919a0c4dec622eb716cf157769eaab9',
    ),
    'prestashop/gsitemap' => 
    array (
      'pretty_version' => 'v4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b67dda02a8b6488eb4e2a67080357ac3f9e57057',
    ),
    'prestashop/pagesnotfound' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8707d8184f0c6678a95c64b957aec6053ed069ef',
    ),
    'prestashop/prestashop' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'prestashop/productcomments' => 
    array (
      'pretty_version' => '4.2.1',
      'version' => '4.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf361efa8b291c5134a2cceeeb1ab84db31b05b7',
    ),
    'prestashop/ps_banner' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9616f7fc9cda997ef1860562d3212fd021205805',
    ),
    'prestashop/ps_categorytree' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3a23af738d7d0e7aae1fdce0f0de392041719df',
    ),
    'prestashop/ps_checkpayment' => 
    array (
      'pretty_version' => 'v2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '6239ecbe75c427f81536ed2df8b56421ab2fa9b2',
    ),
    'prestashop/ps_contactinfo' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c74f18a12118cbb7622e79044c48aaa55ce93c3',
    ),
    'prestashop/ps_crossselling' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '09194273b73c1d45d950f645a6ddb24330615bfb',
    ),
    'prestashop/ps_currencyselector' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e2b87c4fbddf757af0233ab419e8a4a76d4daf0',
    ),
    'prestashop/ps_customeraccountlinks' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d9d4c87697a00f2d00def427d9f7058e14bca94',
    ),
    'prestashop/ps_customersignin' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9006ddf5a3a376e60bf300eecf1608286f739ca',
    ),
    'prestashop/ps_customtext' => 
    array (
      'pretty_version' => 'v4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d46ad28d99ec03dbba31d121cce1365593425c6',
    ),
    'prestashop/ps_dataprivacy' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7635ac3a3d373c26af81d28ead8efbf76ba53796',
    ),
    'prestashop/ps_emailsubscription' => 
    array (
      'pretty_version' => 'v2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4ef6d74fe3dd3f2f3a0cd7c7c6b92bf0f9277f6',
    ),
    'prestashop/ps_facetedsearch' => 
    array (
      'pretty_version' => 'v3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a889cce83d7df567e1f6d1b1ebf16915a102a5ba',
    ),
    'prestashop/ps_faviconnotificationbo' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '161cdd57a520d8a0acd3dac96b427acc178dd9d7',
    ),
    'prestashop/ps_featuredproducts' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a175279ce8fce9669fa1b22c5a13be86c4c1c334',
    ),
    'prestashop/ps_imageslider' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '52e9d1a068e3b2879ce2184f00c083c8190c7ce7',
    ),
    'prestashop/ps_languageselector' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '22d69b4dbc1a12ab2692d91962d8993d6e64557f',
    ),
    'prestashop/ps_linklist' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4abf07d4ee2b007df78849fc9475c2dcab087a69',
    ),
    'prestashop/ps_mainmenu' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '23323e6165423a58b2b79edd7a13a4de338d922f',
    ),
    'prestashop/ps_searchbar' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae165e9ca469926df0b07181bf8ecee431f82070',
    ),
    'prestashop/ps_sharebuttons' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a0302774b84820be38e76a34188d777de12403f',
    ),
    'prestashop/ps_shoppingcart' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea54698cfb4324508ae0c89efe737c1c3c03f17',
    ),
    'prestashop/ps_socialfollow' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a1bb73cbcdcf929b876b624f033d87c8b101133',
    ),
    'prestashop/ps_themecusto' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '69482091a23a68caa19aedc89f04f01fa997db77',
    ),
    'prestashop/ps_wirepayment' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '99e8fbb3c66bad0217cf6f19a987d4f175aa2740',
    ),
    'prestashop/sekeywords' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f174c055da105af3054690a4f6c48208849b5241',
    ),
    'prestashop/statsbestcategories' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '57791a21f63d43cf145fa1ef1f84031a47f5e7c2',
    ),
    'prestashop/statsbestcustomers' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b0250e88e9144ca373d8006c922d3e84ea4987b9',
    ),
    'prestashop/statsbestmanufacturers' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9897beda6084d08f489d4d00995a085b8e76739',
    ),
    'prestashop/statsbestproducts' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ed33cdcc6cc6657ad97b944620e085a36261bc8',
    ),
    'prestashop/statsbestsuppliers' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e46e732b8a77dc989f5891ff9d3616d1c5fd46f',
    ),
    'prestashop/statsbestvouchers' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8ee93efa6d83cca3410285d8a7f4df904db21f0',
    ),
    'prestashop/statscarrier' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ccfe3914dd19f1fa31166e001c55eefe574877d',
    ),
    'prestashop/statscatalog' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be6be71c703900dda0182e56c9c6658ab85f215b',
    ),
    'prestashop/statscheckup' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7acd7702ab8ecb2b88e529e1eab8d4b7fafbca07',
    ),
    'prestashop/statsdata' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4de78d4c0b096f2d6768f5c96856ea8eabd8358c',
    ),
    'prestashop/statsequipment' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b0c3f75d55bdd18b2903f8263ce3b48b6cf30ec',
    ),
    'prestashop/statsforecast' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ecbeaef0e32657cf79000f95861e41e19e22d49',
    ),
    'prestashop/statslive' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a15002cbae3f68ffcdde015c50662a4dc4abcde',
    ),
    'prestashop/statsnewsletter' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7fd9a55c4b39eb034f73ef4723a1a83c6d73560e',
    ),
    'prestashop/statsorigin' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '24ca4f143a2f0b5d3d7bc817547df79f837faeb3',
    ),
    'prestashop/statspersonalinfos' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd00510d282fc442535f8110f30f44d502c4addaa',
    ),
    'prestashop/statsproduct' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'caf05aefabd63ac84e06bc2a0bdcf69f965d03c4',
    ),
    'prestashop/statsregistrations' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '51d79fb12770919f1b1f9d9d9f931148d60149c2',
    ),
    'prestashop/statssales' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e28ce4ea2eb1a4a8b6b76bfb6b56e1b02ef651a',
    ),
    'prestashop/statssearch' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b77131a1fdbebc4d5d6768a31de17b4d2a26b97',
    ),
    'prestashop/statsstock' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b60aae430151943d6e4b0068a3e30ab2cc5843e',
    ),
    'prestashop/statsvisits' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '95aa7494f4329dcf8e0dd0386eb6513eb2976f44',
    ),
    'prestashop/translationtools-bundle' => 
    array (
      'pretty_version' => 'v4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba663b159bd560a2b570766ff5610c81f2462035',
    ),
    'prestashop/welcome' => 
    array (
      'pretty_version' => 'v6.0.4',
      'version' => '6.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '2634da35dec6c52a8c3a18dfcf051dbbb6b48edc',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '446d54b4cb6bf489fc9d75f55843658e6f25d801',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31ffa96f8d2ed0341a57848cbb84d88b89dd664d',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'rsky/pear-core-min' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.10.10',
      ),
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'sensio/distribution-bundle' => 
    array (
      'pretty_version' => 'v5.0.25',
      'version' => '5.0.25.0',
      'aliases' => 
      array (
      ),
      'reference' => '80a38234bde8321fb92aa0b8c27978a272bb4baf',
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v5.4.1',
      'version' => '5.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '585f4b3a1c54f24d1a8431c729fc8f5acca20c8a',
    ),
    'sensiolabs/security-checker' => 
    array (
      'pretty_version' => 'v6.0.3',
      'version' => '6.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a576c01520d9761901f269c4934ba55448be4a54',
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shudrum/array-finder' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42380f01017371b7a1e8e02b0bf12cb534e454d7',
    ),
    'smarty/smarty' => 
    array (
      'pretty_version' => 'v3.1.34',
      'version' => '3.1.34.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9f0de05f41b9e52798b268ab1e625fac3b8578c',
    ),
    'soundasleep/html2text' => 
    array (
      'pretty_version' => '0.5.0',
      'version' => '0.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cdb89f6ffa2c4cc78f8ed9ea6ee0594a9133ccad',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
    ),
    'symfony/asset' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/browser-kit' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/class-loader' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/css-selector' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/debug' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/debug-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/dependency-injection' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/doctrine-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/dom-crawler' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/dotenv' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/event-dispatcher' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/finder' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/form' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/framework-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/http-client' => 
    array (
      'pretty_version' => 'v4.4.2',
      'version' => '4.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ebfc77b5018a05226b38642def82746f3e2ce0f',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v1.1.8',
      'version' => '1.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '088bae75cfa2ec5eb6d33dce17dbd8613150ce6e',
    ),
    'symfony/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/http-kernel' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/inflector' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/intl' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/ldap' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/lock' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.2',
      'version' => '4.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '010cc488e56cafe5f7494dea70aea93100c234df',
    ),
    'symfony/monolog-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd80460fcfe1fa2050a7103ad818e9d0686ce6fd',
    ),
    'symfony/options-resolver' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v3.4.36',
      'version' => '3.4.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cbea8818e9f34e4e9d780bd22bdda21b57d4d5c7',
    ),
    'symfony/polyfill-apcu' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8e961c841b9ec52927a87914f8820a1ad8f8116',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f8f0b461be3385e56d6de3dbb5a0df24c0c275e3',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a019efccc03f1a335af6b4f20c30f5ea8060be36',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3dffd68afa61ca70f2327f2dd9bbeb6aa53d70b',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f9c239e61e1b0c9229a28ff89a812dc449c3d46',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b4aab9743c30be783b73de055d24a39cf4b954f',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '53dd1cdf3cb986893ccf2b96665b25b3abb384f4',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af23c7bb26a73b850840823662dda371484926c4',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '66fea50f6cb37a35eea048d75a7d99a45b586038',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b0e2222c55a25b4541305a053013d5647d3a25f',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '964a67f293b66b95883a5ed918a65354fcd2258f',
    ),
    'symfony/process' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/property-access' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/property-info' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security-core' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security-csrf' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security-guard' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/security-http' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/serializer' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.8',
      'version' => '1.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ffc7f5692092df31515df2a5ecf3b7302b3ddacf',
    ),
    'symfony/stopwatch' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/swiftmailer-bundle' => 
    array (
      'pretty_version' => 'v3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a83160b50a2479d37eb74ba71577380b9afe4f5',
    ),
    'symfony/symfony' => 
    array (
      'pretty_version' => 'v3.4.37',
      'version' => '3.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '1bd873459b36cf505c7b515ba6e0e2ee40890b8a',
    ),
    'symfony/templating' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/twig-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/validator' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/web-link' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/web-server-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/workflow' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'symfony/yaml' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.4.37',
      ),
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.3.2',
      'version' => '6.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9fde7bb9b404b945e7ea88fb7eccd23d9a4e324b',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '11336f6f84e16a720dae9d8e6ed5019efa85a0f9',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dda2ee426acd6d801d5b7fd1001cde9b5f790e15',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.42.4',
      'version' => '1.42.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e587180584c3d2d6cb864a0454e777bb6dcb6152',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '573381c0a64f155a0d9a23f4b0c797194805b925',
    ),
    'willdurand/jsonp-callback-validator' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a7d388bb521959e612ef50c5c7b1691b097e909',
    ),
  ),
);
