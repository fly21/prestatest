/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */
import 'expose-loader?Tether!tether';
import 'bootstrap/dist/js/bootstrap.min';
import 'flexibility';
import 'bootstrap-touchspin';
import 'jquery-touchswipe';

import './responsive';
import './checkout';
import './customer';
import './listing';
import './product';
import './cart';

import prestashop from 'prestashop';
import EventEmitter from 'events';
import DropDown from './components/drop-down';
import Form from './components/form';
import ProductMinitature from './components/product-miniature';
import ProductSelect from './components/product-select';
import TopMenu from './components/top-menu';

import './lib/bootstrap-filestyle.min';
import './lib/jquery.scrollbox.min';

import './components/block-cart';
import $ from 'jquery';
import getQueryParameters from "../../../../modules/ps_facetedsearch/_dev/front/urlparser";
import refreshSliders from "../../../../modules/ps_facetedsearch/_dev/front/slider";

// "inherit" EventEmitter
for (const i in EventEmitter.prototype) {
  prestashop[i] = EventEmitter.prototype[i];
}

$(document).ready(() => {
  const dropDownEl = $('.js-dropdown');
  const form = new Form();
  const topMenuEl = $('.js-top-menu ul[data-depth="0"]');
  const dropDown = new DropDown(dropDownEl);
  const topMenu = new TopMenu(topMenuEl);
  const productMinitature = new ProductMinitature();
  const productSelect = new ProductSelect();
  dropDown.init();
  form.init();
  topMenu.init();
  productMinitature.init();
  productSelect.init();

  $('.carousel[data-touch="true"]').swipe({
    swipe(event, direction, distance, duration, fingerCount, fingerData) {
      if (direction == 'left') {
        $(this).carousel('next');
      }
      if (direction == 'right') {
        $(this).carousel('prev');
      }
    },
    allowPageScroll: 'vertical',
  });
});

$(document).ready(() => {
    $('body').on('click', '.show_list', function () {
        document.cookie = "show_list=true; expires=Thu, 30 Jan 2100 12:00:00 UTC; path=/";
        $('#js-product-list .product-miniature').addClass('product_show_list').parent().css('width', '100%');
    });
    $('body').on('click', '.show_grid', function () {
        document.cookie = "show_list=; expires=Thu, 30 Jan 1970 12:00:00 UTC; path=/";
        $('#js-product-list .product-miniature').removeClass('product_show_list').parent().css('width', 'auto');
    });

  prestashop.on('updateProductList', function (event) {
    $('.show_list').click(function(){
      $('#js-product-list .product-miniature').addClass('product_show_list');
    });

    $('.show_grid').click(function(){
      $('#js-product-list .product-miniature').removeClass('product_show_list');
    });
  });

  $('#js-product-list .product_show_list').parent().css('width', '100%');

  $('body').on('change submit', '.filter_input_start, .filter_input_finish', function () {
    const slider_div = $('.faceted-slider.price_slider');
    const slider_div_id = slider_div.data('slider-id');
    $('#slider-range_'+slider_div_id).slider('values' , [parseInt($('.filter_input_start').val()), parseInt($('.filter_input_finish').val())]);
    updateSliders();
  });
});

function updateSliders() {
  const slider_div = $('.faceted-slider.price_slider');
  const nextEncodedFacetsURL = slider_div.data('slider-encoded-url');
  const urlsSplitted = nextEncodedFacetsURL.split('?');
  let queryParams = [];

  // Retrieve parameters if exists
  if (urlsSplitted.length > 1) {
    queryParams = getQueryParameters(urlsSplitted[1]);
  }

  let found = false;
  queryParams.forEach(function (query) {
    if (query.name === 'q') {
      found = true;
    }
  });

  if (!found) {
    queryParams.push({name: 'q', value: ''});
  }

  // Update query parameter
  queryParams.forEach(function (query) {
    if (query.name === 'q') {
      // eslint-disable-next-line
      query.value += [
        query.value.length > 0 ? '/' : '',
        slider_div.data('slider-label'),
        '-',
        slider_div.data('slider-unit'),
        '-',
        parseInt($('.filter_input_start').val()),
        '-',
        parseInt($('.filter_input_finish').val()),
      ].join('');
    }
  });

  const requestUrl = [
    urlsSplitted[0],
    '?',
    $.param(queryParams),
  ].join('');

  prestashop.emit(
      'updateFacets',
      requestUrl,
  );
}